from django import forms
from.models import Estudio, Pacientes

#en esta parte se genera el back end de los formularios web 
#generando otra forma de cargar los datos desde la pagina web

class estudioFormu(forms.ModelForm):
    class Meta:
        model=Estudio # aca indicamos los campos que vamos a utilizar 

        fields= [
            'pacientes',         #se realizo cambio en el formulario con la relacion de la tabla paciente y estudio
            'instituciones',
            'medico',
            'peso',     
            'talla',
            'indicacion',
            'decubito',
            'perimetroAbdominal',
            'edad',
            'urgencia',
            'imc',
            'supCorporal',
            'calidadTecnica',
            'observaciones',
        ]
        labels = {
            'pacientes':'Pacientes',         #se realizo cambio en el formulario con la relacion de la tabla paciente y estudio
            'instituciones':'Instituciones',
            'medico':'Medico',
            'peso':'Peso',     
            'talla':'Talla',
            'indicacion':'Indicacion',
            'decubito':'Decubito',
            'perimetroAbdominal':'Perimetro abdominal',
            'edad':'Edad',
            'urgencia':'Urgencia',
            'imc':'Imc',
            'supCorporal':'Superficie corporal',
            'calidadTecnica':'Calidad tecnica',
            'observaciones':'Observaciones',
        }
        
        widgets={
            'pacientes':forms.Select(attrs={'class':'form-control bordes'}),
            'instituciones':forms.Select(attrs={'class':'form-control bordes'}),
            'medico':forms.TextInput(attrs={'class':'form-control bordes'}),
            'peso':forms.TextInput(attrs={'class':'form-control bordes','required':'required' ,'onKeyUp':'calcular()','id':'num1'}),
            'talla':forms.TextInput(attrs={'class':'form-control bordes', 'onKeyUp':'calcular()', 'id':'num2'}),
            'indicacion':forms.TextInput(attrs={'class':'form-control bordes'}),
            'decubito':forms.TextInput(attrs={'class':'form-control bordes'}),
            'perimetroAbdominal':forms.TextInput(attrs={'class':'form-control bordes'}),
            'edad':forms.TextInput(attrs={'class':'form-control bordes'}),
            'urgencia':forms.CheckboxInput(attrs={'class':'form-control ','style':'width:30px;'}),
            'imc':forms.TextInput(attrs={'class':'form-control bordes','id':'resultado','readonly':''}),
            'supCorporal':forms.TextInput(attrs={'class':'form-control bordes', 'id':'resultado2','readonly':''}),
            'calidadTecnica':forms.TextInput(attrs={'class':'form-control '}),
            'observaciones':forms.TextInput(attrs={'class':'form-control bordes'}),
        }

#crear Formulario de los pacientes.

class pacienteFormu(forms.ModelForm):
    class Meta:
        model=Pacientes

        fields=[
            'nombre',
            'apellido',
            'dni',
            'nacionalidad',
            'domicilio',
            'numero_domicilio',
            'cobertura_medica',
            'numero_afilido',
            'sexo',
            'fecha_nacimiento',

        ]

        labels={
            'nombre': 'Nombre',
            'apellido': 'Apellido',
            'dni':'Dni',
            'nacionalidad':'Nacionalidad',
            'domicilio':'Domicilio',
            'numero_domicilio':'Numero de domicilio',
            'cobertura_medica':'Cobertura medica',
            'numero_afilido':'Numero de afiliado',
            'sexo':'Sexo',
            'fecha_nacimiento':'Fecha de nacimiento',
        }

        widgets={
            'nombre':forms.TextInput(attrs={'class':'form-control bordes'}),
            'apellido':forms.TextInput(attrs={'class':'form-control bordes'}),
            'dni':forms.TextInput(attrs={'class':'form-control bordes'}),
            'nacionalidad':forms.TextInput(attrs={'class':'form-control bordes'}),
            'domicilio':forms.TextInput(attrs={'class':'form-control bordes'}),
            'numero_domicilio':forms.TextInput(attrs={'class':'form-control bordes'}),
            'cobertura_medica':forms.TextInput(attrs={'class':'form-control bordes'}),
            'numero_afilido':forms.TextInput(attrs={'class':'form-control bordes'}),
            'sexo':forms.TextInput(attrs={'class':'form-control bordes'}),
            'fecha_nacimiento':forms.DateInput(attrs={'class':'form-control bordes'}),
        }


