from django import forms
from.models import FuncionVentricular

class ventricularFormu(forms.ModelForm):
    class Meta:
        model=FuncionVentricular # aca indicamos los campos que vamos a utilizar 
        fields= [

            'paciente_fv', 
            'estudio',
            'vol_relat_rest', 
            'vfdvi_rest', 
            'fvsvi_rest', 
            'vfdvi_m2_rest', 
            'fvsvi_m2_rest', 
            'frac_eyeccion_rest',  
            'vol_relat_stress', 
            'vfdvi_stress',
            'fvsvi_stress',
            'vfdvi_m2_stress',
            'fvsvi_m2_stress',
            'frac_eyeccion_stress',
            'tid',
        ]

        labels = {
            'paciente_fv':'Paciente', 
            'estudio':'estudio',
            'vol_relat_rest':'vol relat rest', 
            'vfdvi_rest':'vfdvi rest', 
            'fvsvi_rest':'fvsvi_rest', 
            'vfdvi_m2_rest':'vfdvi_m2_rest', 
            'fvsvi_m2_rest':'fvsvi_m2_rest', 
            'frac_eyeccion_rest':'frac_eyeccion_rest',  
            'vol_relat_stress':'vol_relat_stress', 
            'vfdvi_stress':'vfdvi_stress',
            'fvsvi_stress':'fvsvi_stress',
            'vfdvi_m2_stress':'vfdvi_m2_stress',
            'fvsvi_m2_stress':'fvsvi_m2_stress',
            'frac_eyeccion_stress':'frac_eyeccion_stress',
            'tid':'tid',
        }

        widgets = {
        'paciente_fv':forms.Select(attrs={'class':'form-control bordes1' ,'style':'height:40px ; display:none;' , 'id':'id_pac_ventricular'}), 
            'estudio':forms.Select(attrs={'class':'form-control bordes1' ,'style':'height:40px; display:none;'}),
            'vol_relat_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivolumen_relativo','onchange':'calcularStress()','maxlength':'3', 'autocomplete':'off'}), 
            'vfdvi_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivfdvi','onchange':'calcularStress()','maxlength':'3', 'autocomplete':'off'}), 
            'fvsvi_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ifvsvi','onchange':'calcularStress()' ,'maxlength':'3', 'autocomplete':'off'}), 
            'vfdvi_m2_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivfdvim2' ,'readonly':'' ,'title':'El calculo de VFDVI m2 se realizara automaticamente.' , 'autocomplete':'off'}), 
            'fvsvi_m2_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ifvsvim2' ,'readonly':'' ,'title':'El calculo de FVSVI m2 se realizara automaticamente.' , 'autocomplete':'off'}), 
            'frac_eyeccion_rest':forms.TextInput(attrs={'class':'form-control bordes1','id':'ifey' ,'readonly':'' ,'title':'El calculo de FEY: % se realizara automaticamente.'}),  
            'vol_relat_stress':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivolumen_relativo_r','onchange':'calcularStress()','maxlength':'3', 'autocomplete':'off'}), 
            'vfdvi_stress':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivfdvir','onchange':'calcularStress()','maxlength':'3', 'autocomplete':'off'}),
            'fvsvi_stress':forms.TextInput(attrs={'class':'form-control bordes1','id':'ifvsvir','onchange':'calcularStress()','maxlength':'3', 'autocomplete':'off'}),
            'vfdvi_m2_stress':forms.TextInput(attrs={'class':'form-control bordes1','id':'ivfdvirm2','readonly':'' ,'title':'El calculo de VFDVIR m2 se realizara automaticamente.'}),
            'fvsvi_m2_stress':forms.TextInput(attrs={'class':'form-control bordes1','id':'ifvsvirm2','readonly':'' ,'title':'El calculo de FVSVIR m2 se realizara automaticamente.'}),
            'frac_eyeccion_stress':forms.TextInput(attrs={'class':'form-control bordes1','readonly':'','title':'El calculo de FEY R: % se realizara automaticamente.','id':'ifeyr'}),
            'tid':forms.TextInput(attrs={'class':'form-control bordes1','id':'itid','readonly':'' ,'title':'El calculo de TID: % se realizara automaticamente.','maxlength':'3', 'autocomplete':'off'}),
        }